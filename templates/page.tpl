{% extends "base.tpl" %}
{% block metas %}
  {{ super() }}
  <meta name="Description" content="{{ page.description }}">
  <meta name="Keywords" content="{{ page.keywords }}">
{% endblock %}
  
{% block title %}{{ page.title }}{% endblock %}

{% macro render_menuitem(it) -%}
    {% if it.url != 'index' %}
        {% if not 'children' in it %}
        <li><a href="{{ url_for('content', name=it.url) }}">{{ it.title }}</a></li>
        {% else %}
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ it.url }} <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                {% for c in it.children -%}
                {{ render_menuitem(c) }}
                {% endfor %}
            </ul>
        </li>
        {% endif %}
    {% endif %}
{% endmacro %}

{% block navbar %}
<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            {% for m in menu -%}
            {{ render_menuitem(m) }}
            {% endfor %}
          </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
{% endblock %}

{% block content %}
<div class="container">
<h2>{{ page.title }}</h2>
{{ page.html|safe }}
</div>
{% endblock %}