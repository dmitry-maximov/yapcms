from flask_flatpages import FlatPages
import werkzeug
import os


class MyFlatPages(FlatPages):
    @werkzeug.cached_property
    def _pages(self):
        """Walk the page root directory an return a dict of unicode path:
        page object.
        """
        def _walk(directory, path_prefix=()):
            """Walk over directory and find all possible flatpages, files which
            ended with ``FLATPAGES_EXTENSION`` value.
            """
            for name in os.listdir(directory):
                full_name = os.path.join(directory, name)

                if os.path.isdir(full_name):
                    _walk(full_name, path_prefix + (name,))
                else:
                    for extension in extensions:
                        if name.endswith(extension):
                            name_without_extension = name[:-len(extension)]
                            path = u'/'.join(path_prefix + (name_without_extension, ))
                            pages[path] = self._load_file(path, full_name)
                            if extension == '.html':
                                pages[path].html_renderer = lambda x: x
                            pages[path].extension = extension

        # Extensions plural.
        extensions = []
        if isinstance(self.config('extension'), basestring):
            extensions.append(self.config('extension'))
        else:
            extensions = self.config('extension')
        
        pages = {}
        # Fail if the root is a non-ASCII byte string. Use Unicode.
        _walk(unicode(self.root))

        return pages
