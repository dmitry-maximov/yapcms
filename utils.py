import os
import sys


def path_to_dict(path, remove_prefix=None, flatpages=None):
    url = os.path.abspath(path)
    if remove_prefix:
        url = url.replace(remove_prefix + os.sep, '')
    if sys.platform in ['win32', 'cygwin']:
        url = url.replace(os.sep, '/').decode('cp1251')
    else:
        url = url.decode('utf-8')
    
    d = {'url': url}

    if os.path.isdir(path):
        d['type'] = "directory"
        d['children'] = []
        for x in os.listdir(path):
            c = path_to_dict(os.path.join(path, x), remove_prefix, flatpages)
            if c:
                d['children'].append(c)
    else:
        d['type'] = "file"
        d['url'] = os.path.splitext(d['url'])[0]
        if flatpages is not None:
            fp = flatpages.get(d['url'])
            if fp is None:
                d = None
            else:
                d['title'] = fp['title'] if 'title' in fp.meta else d['url']
    return d
