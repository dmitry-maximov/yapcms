import sys
from flask import Flask, render_template, url_for
from flask_flatpages import pygments_style_defs
from flask_frozen import Freezer
from flask.ext.admin import Admin
from jinja2 import Template

from flask.ext.bootstrap import Bootstrap
#from flask.ext.admin.contrib.fileadmin import FileAdmin
from flask_debugtoolbar import DebugToolbarExtension

from myfileadmin import ContentFileAdmin, TemplateFileAdmin
from myflatpages import MyFlatPages

import os.path as op
import utils

#BUILD_MODE = len(sys.argv) > 1 and sys.argv[1] == "build"

DEBUG = True

FLATPAGES_AUTO_RELOAD = DEBUG
FLATPAGES_EXTENSION = ['.md', '.html', '.tpl']
#FLATPAGES_HTML_RENDERER = lambda x: x
FLATPAGES_ROOT = 'content3'
TEMPLATES_ROOT = 'templates'
BOOTSTRAP_SERVE_LOCAL = True
SECRET_KEY = 'a unique and long key'
DEBUG_TB_INTERCEPT_REDIRECTS = False
DEBUG_TB_PROFILER_ENABLED = True

app = Flask(__name__, template_folder=TEMPLATES_ROOT)
app.config.from_object(__name__)

flatpages = MyFlatPages(app)
bootstrap = Bootstrap(app)

#if BUILD_MODE:
#    freezer = Freezer(app)
#else:
toolbar = DebugToolbarExtension(app)
admin = Admin(app)
content_mfa = ContentFileAdmin(op.join(op.dirname(__file__), FLATPAGES_ROOT), '/{}/'.format(FLATPAGES_ROOT), name='Content Files', endpoint='content')
template_mfa = TemplateFileAdmin(op.join(op.dirname(__file__), TEMPLATES_ROOT), '/{}/'.format(TEMPLATES_ROOT), name='Template Files', endpoint='templates')
admin.add_view(content_mfa)
admin.add_view(template_mfa)


@app.route('/pygments.css')
def pygments_css():
    return pygments_style_defs('tango'), 200, {'Content-Type': 'text/css'}


@app.route('/')
@app.route('/<path:name>/')
def content(name='index'):
    # if op.isdir('{}/{}'.format(FLATPAGES_ROOT, name)):
    #     posts = [p for p in flatpages if p.path.startswith(name)]
    #     posts.sort(key=lambda item: item['date'] if 'date' in item else 1, reverse=False)
    #     #print posts
    #     return render_template('dir.tpl', posts=posts)
    # else:
    #print json.dumps(menu, indent=2)

    menu = utils.path_to_dict(FLATPAGES_ROOT, op.abspath(FLATPAGES_ROOT), flatpages)
    page = flatpages.get_or_404(name)
    with app.test_request_context():
        static_url = url_for('static', filename='assets/')
    if page.extension == '.md':
        tpl = render_template('md.tpl', page=page.html)
    else:
        tpl = Template(page.html).render(static_url=static_url)

    #return tpl.render(static_url=static_url)
    return render_template('cr.tpl',
                           current=name,
                           content=tpl,
                           static_url=static_url,
                           menu=menu['children'])


if __name__ == "__main__":
    #if BUILD_MODE:
    #    freezer.freeze()
    #else:
    app.run(host='0.0.0.0', port=8000, debug=DEBUG)
