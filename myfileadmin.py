from flask.ext.admin.base import expose
from flask.ext.admin.contrib.fileadmin import FileAdmin, NameForm
from flask import flash, redirect, request, url_for
from wtforms import fields, validators
from flask.ext.admin import form, helpers
from flask.ext.admin.babel import gettext, lazy_gettext
import os.path as op
import os
import itertools
import yaml
from werkzeug import secure_filename


class MyEditForm(form.BaseForm):
    title = fields.StringField(lazy_gettext('Title'),
                               (validators.required(),))
    keywords = fields.StringField(lazy_gettext('Keywords'))
    description = fields.StringField(lazy_gettext('Description'))
    content = fields.TextAreaField(lazy_gettext('Content'),
                                   (validators.required(),))


class ContentFileAdmin(FileAdmin):
    editable_extensions = ('md', 'html')
    list_template = 'admin/file/list_with_create.html'
    edit_template = 'admin/file/content_edit.html'
    can_create = True

    @expose('/create/', methods=('GET', 'POST'))
    @expose('/create/<path:path>', methods=('GET', 'POST'))
    def create(self, path=None):
        base_path, directory, path = self._normalize_path(path)

        if not self.can_create:
            flash(gettext('File creation is disabled.'), 'error')
            return redirect(self._get_dir_url('.index', path))

        if not self.is_accessible_path(path):
            flash(gettext('Permission denied.'))
            return redirect(self._get_dir_url('.index'))

        form = NameForm(helpers.get_form_data())
        if helpers.validate_form_on_submit(form):
            try:
                filename = op.join(directory, secure_filename(form.name.data))

                if op.exists(filename):
                    flash(gettext('File "%(name)s" already exists.', name=filename), 'error')
                else:
                    open(filename, 'w')
                return redirect(self._get_dir_url('.index', path))
            except Exception as ex:
                flash(gettext('Failed to create file: %(error)s', error=ex), 'error')

        return self.render(self.upload_template, form=form)

    @expose('/edit/', methods=('GET', 'POST'))
    def edit(self):
        path = request.args.getlist('path')
        next_url = None
        if not path:
            return redirect(url_for('.index'))

        if len(path) > 1:
            next_url = url_for('.edit', path=path[1:])
        path = path[0]

        base_path, full_path, path = self._normalize_path(path)

        if not self.is_accessible_path(path) or not self.is_file_editable(path):
            flash(gettext('Permission denied.'))
            return redirect(self._get_dir_url('.index'))

        dir_url = self._get_dir_url('.index', os.path.dirname(path))
        next_url = next_url or dir_url

        form = MyEditForm(helpers.get_form_data())
        error = False

        if helpers.validate_form_on_submit(form):
            form.process(request.form, content='')
            if form.validate():
                try:
                    with open(full_path, 'w') as f:
                        for field in form:
                            if field.name != 'content' and field.data:
                                f.write('{}: {}\n'.format(field.name, field.data.encode('utf-8')))
                        f.write('\n{}'.format(form.content.data.encode('utf-8').replace('\r', '')))
                except IOError:
                    flash(gettext("Error saving changes to %(name)s.", name=path), 'error')
                    error = True
                else:
                    self.on_edit_file(full_path, path)
                    flash(gettext("Changes to %(name)s saved successfully.", name=path))
                    return redirect(next_url)
        else:
            try:
                with open(full_path, 'r') as f:
                    content = f.read()
            except IOError:
                flash(gettext("Error reading %(name)s.", name=path), 'error')
                error = True
            except:
                flash(gettext("Unexpected error while reading from %(name)s", name=path), 'error')
                error = True
            else:
                try:
                    content = content.decode('utf8')
                    lines = iter(content.split(u'\n'))
                    meta_yaml = u'\n'.join(itertools.takewhile(unicode.strip, lines))
                    meta = yaml.safe_load(meta_yaml)
                    if not meta or not isinstance(meta, dict):
                        meta = {}
                    content = u'\n'.join(lines)
                except UnicodeDecodeError:
                    flash(gettext("Cannot edit %(name)s.", name=path), 'error')
                    error = True
                except Exception as e:
                    print e
                    flash(gettext("Unexpected error while reading from %(name)s", name=path), 'error')
                    error = True
                else:
                    for m in meta:
                        if m in form:
                            form[m].data = meta[m]
                    form.content.data = content

        return self.render(self.edit_template, dir_url=dir_url, path=path,
                           form=form, error=error)


class TemplateFileAdmin(FileAdmin):
    editable_extensions = ('tpl', 'html')
    edit_template = 'admin/file/template_edit.html'
